package kz.ruslovislo.sb.account.security;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


public class CORSFilterX implements Filter {

    private final List<String> allowedOrigins = Arrays.asList("http://localhost:8888","http://localhost:8080","http://localhost:4200");

    public void destroy() {

    }

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
//        System.out.println("FILTERING....");
        // Lets make sure that we are working with HTTP (that is, against HttpServletRequest and HttpServletResponse objects)
//        if (req instanceof HttpServletRequest && res instanceof HttpServletResponse)
        {
            System.out.println("=====");
            HttpServletRequest request = (HttpServletRequest) req;
            HttpServletResponse response = (HttpServletResponse) res;

            // Access-Control-Allow-Origin
            String origin = request.getHeader("Origin");
            response.setHeader("Access-Control-Allow-Origin", allowedOrigins.contains(origin) ? origin : "*");
            response.setHeader("Vary", "Origin");

            // Access-Control-Max-Age
            response.setHeader("Access-Control-Max-Age", "5600");

            // Access-Control-Allow-Credentials
            response.setHeader("Access-Control-Allow-Credentials", "true");

            // Access-Control-Allow-Methods
            response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");

            // Access-Control-Allow-Headers
            response.setHeader("Access-Control-Allow-Headers",
                    "Authorization, Cookie, Origin, X-Requested-With, Content-Type, Accept, " + "X-CSRF-TOKEN");


            if ("OPTIONS".equals(request.getMethod())) {
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                chain.doFilter(req, res);
            }
        }/*else
            chain.doFilter(req, res);*/




    }

}
