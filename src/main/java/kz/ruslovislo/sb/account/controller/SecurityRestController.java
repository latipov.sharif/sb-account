package kz.ruslovislo.sb.account.controller;

import kz.ruslovislo.sb.account.dto.ChangePasswordDto;
import kz.ruslovislo.sb.account.dto.SessionUserDto;
import kz.ruslovislo.sb.account.dto.UserSignUpDto;
import kz.ruslovislo.sb.account.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
public class SecurityRestController {

    private UserService userService;

    public SecurityRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/myname")
    public ResponseEntity myname(HttpSession session)throws Exception{
        return new ResponseEntity(SessionUserDto.fromSession(session), HttpStatus.OK);
    }

    @PostMapping("/sign-up")
    public ResponseEntity signup(@Validated @RequestBody UserSignUpDto signUpDto)throws Exception{
        signUpDto = userService.signUp(signUpDto);
        return new ResponseEntity(signUpDto, HttpStatus.OK);

    }

    @PostMapping("/change-password")
    public ResponseEntity changePassword(HttpSession session, @Validated @RequestBody ChangePasswordDto changePasswordDto) throws Exception {
        boolean b = userService.changePassword(SessionUserDto.fromSession(session).getId(),changePasswordDto.getOldPassword(),changePasswordDto.getNewPassword());
        if(b)
            return new ResponseEntity(b, HttpStatus.OK);
        else
            return new ResponseEntity(b, HttpStatus.NOT_MODIFIED);
    }


}
