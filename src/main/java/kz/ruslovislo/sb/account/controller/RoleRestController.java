package kz.ruslovislo.sb.account.controller;

import kz.ruslovislo.pojo.table.TableDataRequest;
import kz.ruslovislo.sb.account.dto.*;
import kz.ruslovislo.sb.account.service.RoleService;
import kz.ruslovislo.sb.account.service.UserService;
import kz.ruslovislo.security.MethodAllowedDescription;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
@RequestMapping("/role")
public class RoleRestController {

    private RoleService roleService;

    public RoleRestController(RoleService roleService) {
        this.roleService = roleService;
    }

    @PostMapping("/data")
    @RolesAllowed({"ROL_DATA"})
    @MethodAllowedDescription("Список ролей")
    public ResponseEntity data(@RequestBody TableDataRequest request)throws Exception{
        return new ResponseEntity<Map>(TableDataRequest.pageToRestDatatable(request,roleService.data(request))  , HttpStatus.OK);
    }

    @PostMapping("/create")
    @RolesAllowed({"ROL_CREATE"})
    @MethodAllowedDescription("Создание роли")
    public ResponseEntity create(@Validated(AbstractDto.New.class) @RequestBody RoleDto role)throws Exception{
        role = roleService.create(role);
        return new ResponseEntity(role, HttpStatus.OK);
    }
    @PostMapping("/update")
    @MethodAllowedDescription("Редактирование роли")
    public ResponseEntity update(@Validated(AbstractDto.Exist.class) @RequestBody RoleDto role)throws Exception{
        role = roleService.update(role);
        return new ResponseEntity(role, HttpStatus.OK);
    }
    @GetMapping("/delete")
    @MethodAllowedDescription("Удаление роли")
    public ResponseEntity delete(Long id)throws Exception{
        roleService.delete(id);
        return new ResponseEntity("deleted", HttpStatus.OK);
    }

    @RolesAllowed({"ROLE_FUNCTION_DATA"})
    @MethodAllowedDescription("Список ролевых функций")
    @PostMapping("/function/data")
    public ResponseEntity roleFunctionData(@RequestBody TableDataRequest request)throws Exception{
        return new ResponseEntity<Map>(TableDataRequest.pageToRestDatatable(request,roleService.roleFunctionData(request))  , HttpStatus.OK);
    }

    @RolesAllowed({"ROL_FUNCTION_ADD"})
    @MethodAllowedDescription("Назвачение функции роли")
    @PostMapping("/function/add")
    public ResponseEntity roleFunctionAdd(@Validated(AbstractDto.New.class) @RequestBody RoleFunctionDto roleFunctionDto)throws Exception{
        roleFunctionDto = roleService.addFunctionToRole(roleFunctionDto);
        return new ResponseEntity(roleFunctionDto, HttpStatus.OK);
    }

    @RolesAllowed({"ROL_FUNCTION_DELETE"})
    @MethodAllowedDescription("Удаление функции роли")
    @GetMapping("/function/delete")
    public ResponseEntity roleFunctionAdd(Long roleFunctionId)throws Exception{
        roleService.deleteRoleFunction(roleFunctionId);
        return new ResponseEntity("deleted", HttpStatus.OK);
    }


}
