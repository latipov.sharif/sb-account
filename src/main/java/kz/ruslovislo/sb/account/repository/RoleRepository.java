package kz.ruslovislo.sb.account.repository;

import kz.ruslovislo.jpa.AbstractRepository;
import kz.ruslovislo.sb.account.model.Role;

import java.util.Optional;

public interface RoleRepository extends AbstractRepository<Role> {

    public Optional<Role> findByName(String name);
}
