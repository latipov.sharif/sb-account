package kz.ruslovislo.sb.account.repository;

import kz.ruslovislo.jpa.AbstractRepository;
import kz.ruslovislo.sb.account.model.UserRole;

import java.util.List;
import java.util.Optional;

public interface UserRoleRepository extends AbstractRepository<UserRole> {

    public Optional<UserRole> findByUserIdAndRoleId(Long userId, Long roleId);

    public List<UserRole> findByUserLogin(String userLogin);



}
