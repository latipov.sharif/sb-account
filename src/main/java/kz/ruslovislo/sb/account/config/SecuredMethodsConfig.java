package kz.ruslovislo.sb.account.config;

import kz.ruslovislo.sb.account.model.*;
import kz.ruslovislo.sb.account.repository.*;
import kz.ruslovislo.security.SecuredMethod;
import kz.ruslovislo.security.SecurityUtils;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Component
public class SecuredMethodsConfig implements ApplicationListener<ContextRefreshedEvent> {

    @Value("${ruslovislo.security.packages}")
    private String securityPackages;

    private FunctionRepository functionRepository;

    private RoleRepository roleRepository;

    private RoleFunctionRepository roleFunctionRepository;

    private UserRoleRepository userRoleRepository;

    private UserRepository userRepository;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public SecuredMethodsConfig(FunctionRepository functionRepository, RoleRepository roleRepository,UserRoleRepository userRoleRepository, RoleFunctionRepository roleFunctionRepository, UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.functionRepository = functionRepository;
        this.roleRepository = roleRepository;
        this.userRoleRepository=userRoleRepository;
        this.roleFunctionRepository=roleFunctionRepository;
        this.userRepository=userRepository;
        this.bCryptPasswordEncoder=bCryptPasswordEncoder;
    }

    @SneakyThrows
    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent){

        List<SecuredMethod> securedMethods = new LinkedList<>();
        if(securityPackages!=null){
            String[] pkgs = securityPackages.split(",");
            if(pkgs!=null)
                for(String pkg:pkgs){
                    securedMethods.addAll(SecurityUtils.scanPackage(pkg));
                }
        }

        Role adminRole = null;
        Optional<Role> adminRoleO = roleRepository.findByName("ADMIN");
        if(!adminRoleO.isPresent()){
            adminRole = new Role();
            adminRole.setName("ADMIN");
            adminRole.setDescription("Администратор");
            adminRole = roleRepository.save(adminRole);
        }else
            adminRole = adminRoleO.get();


        for(SecuredMethod securedMethod:securedMethods){
            Optional<Function> function = functionRepository.findByName(securedMethod.getMethod());
            if(!function.isPresent()){
                Function f = new Function();
                f.setName(securedMethod.getMethod());
                f.setDescription(securedMethod.getDescription());
                f = functionRepository.save(f);
                RoleFunction roleFunction = new RoleFunction();
                roleFunction.setFunction(f);
                roleFunction.setRole(adminRole);
                roleFunctionRepository.save(roleFunction);
            }else{
                Optional<RoleFunction> roleFunctionO = roleFunctionRepository.findByRoleIdAndFunctionId(adminRole.getId(), function.get().getId());
                if(!roleFunctionO.isPresent()){
                    RoleFunction roleFunction = new RoleFunction();
                    roleFunction.setFunction(function.get());
                    roleFunction.setRole(adminRole);
                    roleFunctionRepository.save(roleFunction);
                }
            }
        }


        Optional<User> adminO = userRepository.findByLogin("admin@test.kz");
        User admin = null;
        if(!adminO.isPresent()){
            admin = new User();
            admin.setLogin("admin@test.kz");
            admin.setName("Администратор");
            admin.setPassword(bCryptPasswordEncoder.encode("admin"));
            userRepository.save(admin);
        }else{
            admin = adminO.get();
        }

        Optional<UserRole> userRoleO = userRoleRepository.findByUserIdAndRoleId(admin.getId(), adminRole.getId());
        if(!userRoleO.isPresent()){
            UserRole userRole = new UserRole();
            userRole.setUser(admin);
            userRole.setRole(adminRole);
            userRoleRepository.save(userRole);
        }



    }
}
