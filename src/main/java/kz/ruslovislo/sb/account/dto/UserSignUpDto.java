package kz.ruslovislo.sb.account.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
public class UserSignUpDto {

    @Null(groups = {UserSignUpDto.class})
    private Long id;

    @NotNull(groups = {UserSignUpDto.class})
    private String login;

    @NotNull(groups = {UserSignUpDto.class})
    private String password;

    @NotNull(groups = {UserSignUpDto.class})
    private String name;

}

